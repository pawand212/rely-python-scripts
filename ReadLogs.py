#!/usr/bin/python

# Open a file1

import sys,csv

list1 = []
with open(sys.argv[1]) as f:
    datafile = f.readlines()
    for line in datafile:
        if 'End Result' in line:
            list1.append(line[17:].strip())
            
fail = False
for i in list1:
    if i.split(',')[9] == 'Failed':
        print("BP Failed")
        fail = True
        with open('result.csv', 'a') as file1:
            writer = csv.writer(file1)
            writer.writerow(i.split(','))
        break
    
if not fail:
    with open('result.csv', 'a') as file1:
        writer = csv.writer(file1)
        writer.writerow(list1[0].split(','))