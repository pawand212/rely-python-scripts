#!/usr/bin/env python

import xml.dom.minidom,sys

def main():
# use the parse() function to load and parse an XML file
   doc = xml.dom.minidom.parse("./weblogic/domain/config/jdbc/jdbc.xml");

# get a list of XML tags from the document and print each one
   maxCapacity = doc.getElementsByTagName("max-capacity")
   capacity = maxCapacity[0].firstChild.data
   if int(capacity) > 20:
      print("End Result: [BP Group,BP Document,MBP Details,BP Section,ART,Application,{},{},hostname,Passed]".format(sys.argv[1], sys.argv[2]))
   else:
      print("End Result: [BP Group,BP Document,MBP Details,BP Section,ART,Application,{},{},hostname,Failed]".format(sys.argv[1], sys.argv[2]))

main()